# Monado Driver Viewer

This is a tool to aid in the development of monado drivers.  

## Controller Viewer

The controller viewer is to render arrows in accordance with the respective axis.

## Requirements

* python 3.10+
* pipenv
* monado

## Installation

Issue the following command:

```bash
pipenv install
```

TODO - more details on how to get monado setup