import xr
import time
import ctypes
import math
from ctypes import byref, cast, POINTER
from typing import List, Optional

class Math(object):
    class Pose(object):
        @staticmethod
        def identity():
            t = xr.Posef()
            assert t.orientation.w == 1
            return t

        @staticmethod
        def translation(translation: List[float]):
            t = Math.Pose.identity()
            t.position[:] = translation[:]
            return t

        @staticmethod
        def rotate_ccw_about_y_axis(radians: float, translation: List[float]):
            t = Math.Pose.identity()
            t.orientation.x = 0
            t.orientation.y = math.sin(radians * 0.5)
            t.orientation.z = 0
            t.orientation.w = math.cos(radians * 0.5)
            t.position[:] = translation[:]
            return t

class OpenXR:
    def __init__(self):
        available = xr.enumerate_instance_extension_properties()

        self.app_space = None

        print("== OPEN XR ==")
        for prop in available:
            print(prop)
        #self.init_headless_function()
        self.init_headless_object()


        # When this extension is enabled, an application may call CreateSession without an stext:XrGraphicsBinding* structure in its next chain. In this case, the runtime must create a "headless" session that does not interact with the display.
        # In a headless session, the session state should proceed to SESSION_STATE_READY directly from SESSION_STATE_IDLE.
        # In a headless session, the XrSessionBeginInfo::primaryViewConfigurationType must be ignored and may be 0.
        # In a headless session, the session state proceeds to SESSION_STATE_SYNCHRONIZED, then SESSION_STATE_VISIBLE and SESSION_STATE_FOCUSED, after the call to BeginSession. The application does not need to call WaitFrame, BeginFrame, or EndFrame, unlike with non-headless sessions.
        # In a headless session, EnumerateSwapchainFormats must return SUCCESS but enumerate 0 formats.
        # WaitFrame must set XrFrameState::shouldRender to FALSE in a headless session. The VISIBLE and FOCUSED states are only used for their input-related semantics, not their rendering-related semantics, and these functions are permitted to allow minimal change between headless and non-headless code if desired.

    # checkout the following!!!
    # https://gitlab.freedesktop.org/monado/demos/xr-print-tracker/-/blob/main/src/OpenXRPrintTracker.cpp
    def init_headless_function(self):

        self.instance_object = None

        self.instance_create_info=xr.InstanceCreateInfo(enabled_extension_names=[xr.MND_HEADLESS_EXTENSION_NAME])
        self.instance = xr.create_instance(self.instance_create_info)
        print("instance = ",self.instance)

        system_get_info = xr.SystemGetInfo()
        system = xr.get_system(self.instance,system_get_info)
        print("System = ",system)

        sci = xr.SessionCreateInfo(system_id = system)

        session = xr.create_session(self.instance, sci)
        self.session = session

        self.create_info = xr.ReferenceSpaceCreateInfo(
            pose_in_reference_space=Math.Pose.identity(),
        )
        self.create_info.reference_space_type = xr.ReferenceSpaceType.LOCAL
        self.app_space = xr.create_reference_space(session=self.session, create_info = self.create_info)
        self.reference_space = xr.create_reference_space(
            session=self.session, create_info=xr.ReferenceSpaceCreateInfo( reference_space_type=xr.ReferenceSpaceType.STAGE,),
        )

        print("INIT self.app_space = ",self.app_space)

        # Set up the controller pose action
        controller_paths = (xr.Path * 2)(
            xr.string_to_path(self.instance, "/user/hand/left"),
            xr.string_to_path(self.instance, "/user/hand/right"),
        )

        action_set_info = xr.ActionSetCreateInfo(action_set_name="driver_viewer",localized_action_set_name="driver_viewer")

        action_set = xr.create_action_set(self.instance,action_set_info)
        self.action_set = action_set

        controller_pose_action = xr.create_action(
            action_set=action_set,
            create_info=xr.ActionCreateInfo(
                action_type=xr.ActionType.POSE_INPUT,
                action_name="hand_pose",
                localized_action_name="Hand Pose",
                count_subaction_paths=len(controller_paths),
                subaction_paths=controller_paths,
            ),
        )
        suggested_bindings = (xr.ActionSuggestedBinding * 2)(
            xr.ActionSuggestedBinding(
                action=controller_pose_action,
                binding=xr.string_to_path(
                    instance=self.instance,
                    path_string="/user/hand/left/input/grip/pose",
                ),
            ),
            xr.ActionSuggestedBinding(
                action=controller_pose_action,
                binding=xr.string_to_path(
                    instance=self.instance,
                    path_string="/user/hand/right/input/grip/pose",
                ),
            ),
        )
        xr.suggest_interaction_profile_bindings(
            instance=self.instance,
            suggested_bindings=xr.InteractionProfileSuggestedBinding(
                interaction_profile=xr.string_to_path(
                    self.instance,
                    "/interaction_profiles/khr/simple_controller",
                ),
                count_suggested_bindings=len(suggested_bindings),
                suggested_bindings=suggested_bindings,
            ),
        )
        self.action_spaces = [
            xr.create_action_space(
                #session=self.context.session,
                session = session,
                create_info=xr.ActionSpaceCreateInfo(
                    action=controller_pose_action,
                    subaction_path=controller_paths[0],
                ),
            ),
            xr.create_action_space(
                #session=self.context.session,
                session = session,
                create_info=xr.ActionSpaceCreateInfo(
                    action=controller_pose_action,
                    subaction_path=controller_paths[1],
                ),
            ),
        ]

        # attach the session to the action set
        session_action_sets_attach_info = xr.SessionActionSetsAttachInfo(action_sets=[action_set])
        xr.attach_session_action_sets(session,session_action_sets_attach_info)


    def init_headless_object(self):

        instance_object = xr.InstanceObject( enabled_extensions = [
            xr.MND_HEADLESS_EXTENSION_NAME,
            xr.KHR_CONVERT_TIMESPEC_TIME_EXTENSION_NAME
        ])
        self.instance_object = instance_object

        system = xr.SystemObject(instance_object)

        session = xr.SessionObject(system,None)
        self.session = session

        self.create_info = xr.ReferenceSpaceCreateInfo(
            xr.ReferenceSpaceType.VIEW,
            xr.Posef(xr.Quaternionf(0, 0, 0, 1), xr.Vector3f(0, 0, 0))
        )
        self.app_space = xr.create_reference_space(session = session.handle, create_info = self.create_info)
        self.reference_space = xr.create_reference_space(
            session=self.session.handle, create_info=xr.ReferenceSpaceCreateInfo( reference_space_type=xr.ReferenceSpaceType.STAGE,),
        )

        # Set up the controller pose action
        controller_paths = (xr.Path * 2)(
            xr.string_to_path(instance_object.handle, "/user/hand/left"),
            xr.string_to_path(instance_object.handle, "/user/hand/right"),
        )

        action_set_info = xr.ActionSetCreateInfo(action_set_name="driver_viewer",localized_action_set_name="driver_viewer")

        action_set = xr.create_action_set(instance_object.handle,action_set_info)
        self.action_set = action_set

        controller_pose_action = xr.create_action(
            action_set=action_set,
            create_info=xr.ActionCreateInfo(
                action_type=xr.ActionType.POSE_INPUT,
                action_name="hand_pose",
                localized_action_name="Hand Pose",
                count_subaction_paths=len(controller_paths),
                subaction_paths=controller_paths,
            ),
        )
        suggested_bindings = (xr.ActionSuggestedBinding * 2)(
            xr.ActionSuggestedBinding(
                action=controller_pose_action,
                binding=xr.string_to_path(
                    instance=instance_object.handle,
                    path_string="/user/hand/left/input/grip/pose",
                ),
            ),
            xr.ActionSuggestedBinding(
                action=controller_pose_action,
                binding=xr.string_to_path(
                    instance=instance_object.handle,
                    path_string="/user/hand/right/input/grip/pose",
                ),
            ),
        )
        xr.suggest_interaction_profile_bindings(
            instance=instance_object.handle,
            suggested_bindings=xr.InteractionProfileSuggestedBinding(
                interaction_profile=xr.string_to_path(
                    instance_object.handle,
                    "/interaction_profiles/khr/simple_controller",
                    #"/interaction_profiles/nolo/nolo_controller",
                ),
                count_suggested_bindings=len(suggested_bindings),
                suggested_bindings=suggested_bindings,
            ),
        )
        self.action_spaces = [
            xr.create_action_space(
                session = session.handle,
                create_info=xr.ActionSpaceCreateInfo(
                    action=controller_pose_action,
                    subaction_path=controller_paths[0],
                ),
            ),
            xr.create_action_space(
                session = session.handle,
                create_info=xr.ActionSpaceCreateInfo(
                    action=controller_pose_action,
                    subaction_path=controller_paths[1],
                ),
            ),
        ]

        # attach the session to the action set
        session_action_sets_attach_info = xr.SessionActionSetsAttachInfo(action_sets=[action_set])
        xr.attach_session_action_sets(session.handle , session_action_sets_attach_info)

    def time_from_timespec(self, instance: xr.Instance, timespec_time: xr.timespec) -> xr.Time:
        xr_time = xr.Time()
        result = self.pxrConvertTimespecTimeToTimeKHR(
            instance,
            ctypes.pointer(timespec_time),
            ctypes.byref(xr_time),
        )
        result = xr.check_result(result)
        if result.is_exception():
            raise result
        return xr_time
    def update(self):
        try:
            if self.instance_object != None :
                self.session.poll_xr_events()

                xr.wait_frame(session=self.session.handle)  # Helps SteamVR show application name better

                self.pxrConvertTimespecTimeToTimeKHR = ctypes.cast(
                    xr.get_instance_proc_addr(
                        instance=self.instance_object.handle,
                        name="xrConvertTimespecTimeToTimeKHR",
                    ),
                    xr.PFN_xrConvertTimespecTimeToTimeKHR,
                )

                timespecTime = xr.timespec()
                time_float = time.clock_gettime(time.CLOCK_MONOTONIC)
                timespecTime.tv_sec = int(time_float)
                timespecTime.tv_nsec = int((time_float % 1) * 1e9)
                xr_time_now = self.time_from_timespec(self.instance_object.handle, timespecTime)

                view_configuration_type = self.session.view_configuration_type
                #display_time = t
                view_locate_info = xr.ViewLocateInfo(
                    view_configuration_type,
                    xr_time_now,
                    self.session.space.handle,
                )
                views = xr.locate_views(self.session.handle, view_locate_info)
                i = 0
                j = 0
                for view in views:
                    #print(f"view[{i}] = {view}")
                    i += 1
                    if not isinstance(view,xr.ViewState):
                        hmd_view = view[1]
                        hmd_pose = hmd_view.pose
                active_action_set = xr.ActiveActionSet(
                    action_set=self.action_set,
                    subaction_path=xr.NULL_PATH,
                )
                xr.sync_actions(
                    session=self.session.handle,
                    sync_info=xr.ActionsSyncInfo(
                        active_action_sets=[active_action_set],
                    ),
                )
                found_count = 0
                for index, space in enumerate(self.action_spaces):
                    space_location = xr.locate_space(
                        space=space,
                        base_space=self.reference_space,
                        time = xr_time_now
                    )
#                    print(f"space_location.location_flags = {space_location.location_flags}")
                    if space_location.location_flags & xr.SPACE_LOCATION_POSITION_VALID_BIT:
                        found_count += 1
                        if index == 0:
                            ctrl_left_pose = space_location.pose
                        else:
                            ctrl_right_pose = space_location.pose
                            #print(f"[Controller {index}] P{ctrl_pose.position}")
                if found_count == 0:
                    print("no controllers active")
                return [hmd_pose,ctrl_left_pose,ctrl_right_pose]
        except xr.exception.EventUnavailable:
            print("Event Unavaible Exception")
            pass

        finally:
            pass

def main():
    open_xr = OpenXR()

if __name__ == "__main__":
    main()