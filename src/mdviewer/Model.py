from ursina import *

class Arrow:
    """Creates an arrow with the specified color and axis"""

    def __init__(self, parent, color,is_x=False,is_y=False,is_z=False):
        # Create the x-axis arrow
        #self.arrow_body = Entity(parent=parent,model='cube', color=color, scale=(0.05,2,0.05))
        self.arrow_body = Entity(parent=parent,model=Cylinder(8), color=color, scale=(0.05,2,0.05))
        self.arrow_tip = Entity(parent=parent,model=Cone(4), color=color, scale=(0.15,0.15,0.15))
        if is_x:
            self.set_x()
        elif is_y:
            self.set_y()
        else:
            self.set_z()

    def set_x(self):
        self.arrow_body.rotation_z = 90
        self.arrow_tip.rotation_z = 90
        #self.arrow_body.x = 1
        self.arrow_tip.x = 2

    def set_y(self):
        #self.arrow_body.y = 1
        self.arrow_tip.y = 2

    def set_z(self):
        #self.arrow_body.z = 1
        self.arrow_tip.z = 2
        self.arrow_body.rotation_x = 90
        self.arrow_tip.rotation_x = 90

class OrientationArrow:
    def __init__(self):
        self.orientation_arrow = Entity()
        scale_obj = (0.05,2,0.05)

        # Create the x-axis arrow
        self.x_arrow = Arrow(self.orientation_arrow,color.red,is_x=True)
        self.y_arrow = Arrow(self.orientation_arrow,color.green,is_y=True)
        self.z_arrow = Arrow(self.orientation_arrow,color.blue,is_z=True)
        self.arrow_origin = Entity(parent=self.orientation_arrow,model="sphere", color=color.black, scale=(0.10,0.10,0.10))

    def set_position(self, position):
        #print("["+str(position.x)+","+str(position.y)+","+str(position.z)+"]")
        norm = 1000
        # normalize based on range of -3k to 3k
        self.orientation_arrow.x = position.x/norm
        self.orientation_arrow.y = position.y/norm
        self.orientation_arrow.z = position.z/-norm
    
    def set_rotation(self, rotation):
        self.orientation_arrow.rotation_x = rotation.x
        pass
