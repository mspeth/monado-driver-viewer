from ursina import *

class NoloController:
    """Creates a Nolo controller with the specified color and axis"""

    def __init__(self, id=0):
        self.id = id
        self.controller_entity = Entity(model='assets/NOLO_Controller.obj', texture='assets/NOLO_Controller_Texture.png', doubleSided=True, scale=0.01)
        self.controller_entity.x = 0
        self.controller_entity.y = 1
        self.controller_entity.z = 0

        self.controller_entity.rotation_y = 180
        #scale_obj = (0.05,2,0.05)

    def set_position(self, position):
        #print("["+str(position.x)+","+str(position.y)+","+str(position.z)+"]")
        norm = 1000
        # normalize based on range of -3k to 3k
        self.controller_entity.x = position.x/norm
        self.controller_entity.y = position.y/norm
        self.controller_entity.z = position.z/-norm
    
    def set_rotation(self, rotation):
        self.controller_entity.rotation_x = rotation.x
        pass
