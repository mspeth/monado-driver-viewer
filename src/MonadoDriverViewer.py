from ursina import *
from mdviewer.Model import OrientationArrow
from mdviewer.NoloController import NoloController
from ursina.prefabs.first_person_controller import FirstPersonController
from mdviewer.VR import OpenXR

# create a window
app = Ursina()

window.borderless = False

# most things in ursina are Entities. An Entity is a thing you place in the world.
# you can think of them as GameObjects in Unity or Actors in Unreal.
# the first paramenter tells us the Entity's model will be a 3d-model called 'cube'.
# ursina includes some basic models like 'cube', 'sphere' and 'quad'.

# the next parameter tells us the model's color should be orange.

# 'scale_y=2' tells us how big the entity should be in the vertical axis, how tall it should be.
# in ursina, positive x is right, positive y is up, and positive z is forward.

#player = Entity(model='cube', color=color.orange, scale_y=2)
o_arrow = OrientationArrow()
o_arrow.orientation_arrow.rotation_y = -15

left_controller = NoloController(id=0)
right_controller = NoloController(id=1)

ground = Entity(model='plane', collider='box', scale=64, texture='grass', texture_scale=(4,4))

editor_camera = EditorCamera(enabled=False, ignore_paused=True)
player = FirstPersonController(model='cube', z=-10, color=color.orange, origin_y=-.5, speed=8, collider='box')
player.collider = BoxCollider(player, Vec3(0,1,0), Vec3(1,2,1))

for i in range(16):
    Entity(model='cube', origin_y=-.5, scale=2, texture='brick', texture_scale=(1,2),
        x=random.uniform(-8,8),
        z=random.uniform(1,16) + 8,
        collider='box',
        scale_y = random.uniform(2,3),
        color=color.hsv(0, 0, random.uniform(.9, 1))
        )


# Get VR Gear
open_xr = OpenXR()

# create a function called 'update'.
# this will automatically get called by the engine every frame.

def update():
    if held_keys['escape']:
        quit()
    pose_list = open_xr.update()
    hmd_pose = pose_list[0]
    left_controller_pose = pose_list[1]
    right_controller_pose = pose_list[2]
    if hmd_pose != None:
        o_arrow.set_position(hmd_pose.position)
        #o_arrow.set_rotation(pose.orientation)
        #print("["+str(pose.orientation.x)+","+str(pose.orientation.y)+","+str(pose.orientation.z)+"]")
    #pass
    if left_controller_pose != None:
        left_controller.set_position(left_controller_pose.position)
    if right_controller_pose != None:
        right_controller.set_position(right_controller_pose.position)

app.run()
